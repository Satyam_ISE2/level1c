#include <stdio.h>
void input(float *ptrh, float *ptrb, float *ptrd)
{
    printf("Enter h\n");
    scanf("%f",ptrh);
    printf("Enter b\n");
    scanf("%f",ptrb);
    printf("Enter d\n");
    scanf("%f",ptrd);

}

float find_volume(float h, float b, float d)
{
    float volume;
    volume = ((((h*d)+d)*(1.0/3))/b);
    return volume;
}

void output(float volume)
{
    printf("Volume of Tromboloid is %f",volume);
}

int main()
{
    float h,b,d,v;
    input(&h,&b,&d);
    v = find_volume(h,b,d);
    output(v);
    return 0;
}