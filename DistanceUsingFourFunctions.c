#include <stdio.h>
#include <math.h>
void input(float *ptrx1, float *ptry1, float *ptrx2, float *ptry2)
{
    printf("Enter the coordinates of point1\n");
    scanf("%f %f",ptrx1,ptry1);
    printf("Enter the coordinates of point2\n");
    scanf("%f %f",ptrx2,ptry2);

}

float compute(float x1, float y1, float x2, float y2)
{
    float d;
    d = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return d;
}

void output(float x1, float y1, float x2, float y2, float d)
{
    printf("The distance between %f,%f and %f,%f is %f\n",x1,y1,x2,y2,d);
}

int main()
{
    float x1 = 0, x2 = 0, y1 = 0, y2 = 0,z;
    input(&x1, &y1, &x2, &y2);
    z = compute(x1, y1, x2, y2);
    output(x1, y1, x2, y2, z);
    return 0;
}